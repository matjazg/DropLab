var CustomEvent = require('./custom_event_polyfill');
var Hook = require('./hook');

var HookButton = function(trigger, list) {
  Hook.call(this, trigger, list);
  this.type = 'button';
  this.event = 'click';
  this.addEvents();
};

HookButton.prototype = Object.create(Hook.prototype);

Object.assign(HookButton.prototype, {
  addEvents: function(){
    var self = this;
    this.trigger.addEventListener('click', function(e){
      var buttonEvent = new CustomEvent('click.dl', {
        detail: {
          hook: self,
        },
        bubbles: true,
        cancelable: true
      });
      self.list.show();
      e.target.dispatchEvent(buttonEvent);
    });
  },

  constructor: HookButton,
});


module.exports = HookButton;
