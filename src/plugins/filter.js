/* global droplab */
droplab.plugin(function init(DropLab) {

  var keydown = function keydown(e) {
    var list = e.detail.hook.list;
    var data = list.data;
    var value = e.detail.hook.trigger.value.toLowerCase();
    var config;
    var matches = [];
    var visibleMatches = [];
    var notFoundItem = [];
    // will only work on dynamically set data
    if(!data){
      return;
    }
    config = droplab.config[e.detail.hook.id];
    matches = data.map(function(o){
      // cheap string search
      o.droplab_hidden = o[config.text].toLowerCase().indexOf(value) === -1;
      return o;
    });

    visibleMatches = matches.filter(function(item) {
      return !item.droplab_hidden;
    });

    notFoundItem = data.filter(function(item) {
      return item.id === -1;
    });

    if (!visibleMatches.length) {
      if (!notFoundItem.length) {
        droplab.addData('inputter', [{id: -1, text: "No results", droplab_hidden: false}]);
      } else {
        notFoundItem[0].droplab_hidden = false;
      }
    } else if (notFoundItem[0]) {
      notFoundItem[0].droplab_hidden = true;
    }

    list.render(matches);
  }

  window.addEventListener('keyup.dl', keydown);
});
