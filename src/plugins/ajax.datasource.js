/* global droplab */
droplab.plugin(function init(DropLab) {
  var _addData = DropLab.prototype.addData;

  var _loadUrlData = function(url) {
    return new Promise(function(resolve, reject) {
      var xhr = new XMLHttpRequest;
      xhr.open('GET', url, true);
      xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            var data = JSON.parse(xhr.responseText);
            return resolve(data);
          } else {
            return reject([xhr.responseText, xhr.status]);
          }
        }
      };
      xhr.send();
    });
  };

  Object.assign(DropLab.prototype, {
    addData: function(trigger, data) {
      var _this = this;
      if('string' === typeof data) {
        _loadUrlData(data).then(function(d) {
          _addData.call(_this, trigger, d);
        }).catch(function(e) {
          if(e.message)
            console.error(e.message, e.stack); // eslint-disable-line no-console
          else
            console.error(e); // eslint-disable-line no-console
        })
      } else {
        _addData.apply(this, arguments);
      }
    },
  });
});
